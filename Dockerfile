FROM docker.io/library/centos:latest
RUN yum install httpd -y
COPY ./index.html /var/www/html/
CMD ["-D", "FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]
